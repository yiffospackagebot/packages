# Package Maintainers
MAINTAINERS=("Evie Viau <evie@uwueviee.live>")

# Package information
NAME="cmake"
VERSION="3.22.1"
EPOCH=0
DESC="Modern toolset for generating Makefiles."
GRPS=()
URL="https://cmake.org/"
LICENSES=("BSD-3-Clause")
DEPENDS=("curl" "libarchive" "nghttp2" "libuv")
OPT_DEPENDS=()
PROVIDES=("cmake")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://github.com/Kitware/CMake/releases/download/v${VERSION}/cmake-${VERSION}.tar.gz")

SUM_TYPE="sha512"
SUM=("b1e900fe573cd1cc76d26386f2298d7722737c9ff67930ee108994972b4561ef69caeb537177c9b95b7f17b755e20e034825d3807ea0d2dd4c391310b03adc11")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    # Disable /usr/lib64
    sed -i '/"lib64"/s/64//' Modules/GNUInstallDirs.cmake

    ./bootstrap --prefix=/usr                       \
                --system-libs                       \
                --mandir=/share/man                 \
                --no-system-jsoncpp                 \
                --no-system-librhash                \
                --docdir=/share/doc/cmake-${VERSION}

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    make

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    DESTDIR="${BUILD_DATA_ROOT}" make install

    return 0
}
# Package Maintainers
MAINTAINERS=("Evie Viau <evie@uwueviee.live>")

# Package information
NAME="lua"
VERSION="5.4.3"
_MAJOR_VERSION="5.4"
EPOCH=0
DESC="A lightweight programming language designed for extending applications."
GRPS=()
URL="https://www.lua.org/"
LICENSES=("MIT")
DEPENDS=("readline")
OPT_DEPENDS=()
MK_DEPENDS=()
PROVIDES=("lua")
CONFLICTS=()
REPLACES=()

# Source information
SRC=("https://www.lua.org/ftp/lua-${VERSION}.tar.gz"
"https://www.linuxfromscratch.org/patches/blfs/svn/lua-5.4.3-shared_library-1.patch")

SUM_TYPE="sha512"
SUM=("3a1a3ee8694b72b4ec9d3ce76705fe179328294353604ca950c53f41b41161b449877d43318ef4501fee44ecbd6c83314ce7468d7425ba9b2903c9c32a28bbc0"
"593572a370240165827e1a3646f629c0e01147707367faf8846c2fe6322161710174d0dfbdc18ac0e44344704117252dc924efeefe4e9a1b0d7f1465d13ae392")

# Prepare script
function prepare() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    # Create lua pkg-config file
cat > lua.pc << "EOF"
V=${_MAJOR_VERSION}
R=${VERSION}

prefix=/usr
INSTALL_BIN=${prefix}/bin
INSTALL_INC=${prefix}/include
INSTALL_LIB=${prefix}/lib
INSTALL_MAN=${prefix}/share/man/man1
INSTALL_LMOD=${prefix}/share/lua/${V}
INSTALL_CMOD=${prefix}/lib/lua/${V}
exec_prefix=${prefix}
libdir=${exec_prefix}/lib
includedir=${prefix}/include

Name: Lua
Description: An Extensible Extension Language
Version: ${R}
Requires:
Libs: -L${libdir} -llua -lm -ldl
Cflags: -I${includedir}
EOF

    # Fix issues with shared libraries
    patch -Np1 -i ${WORKDIR}/lua-5.4.3-shared_library-1.patch

    return 0
}

# Build script
function build() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    make linux

    make test

    return 0
}

# Post build script
function postbuild() {
    cd "${WORKDIR}/${NAME}-${VERSION}"

    make INSTALL_TOP=${BUILD_DATA_ROOT}                     \
        INSTALL_DATA="cp -d"                                \
        INSTALL_MAN=${BUILD_DATA_ROOT}/share/man/man1       \
        TO_LIB="liblua.so liblua.so.5.4 liblua.so.5.4.3"    \
        install

    mkdir -pv                      ${BUILD_DATA_ROOT}/usr/share/doc/lua-${VERSION}
    cp -v doc/*.{html,css,gif,png} ${BUILD_DATA_ROOT}/usr/share/doc/lua-${VERSION}

    mkdir -pv                  ${BUILD_DATA_ROOT}/usr/lib/pkgconfig
    install -v -m644 -D lua.pc ${BUILD_DATA_ROOT}/usr/lib/pkgconfig/lua.pc

    return 0
}